/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  x5
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "AppDelegate.h"
#import "JustepURLProtocol.h"
#import "MainViewController.h"
#import "BGLoginViewController.h"
#import "CustomNavigationController.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <BMKLocationKit/BMKLocationComponent.h>

#define CLIENT_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

BMKMapManager* _mapManager;
@implementation AppDelegate

- (id)init
{
  /** If you need to do any extra app-specific initialization, you can do it here
   *  -jm
   **/
  NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
  
  [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
  
  int cacheSizeMemory = 8 * 1024 * 1024; // 8MB
  int cacheSizeDisk = 32 * 1024 * 1024; // 32MB
#if __has_feature(objc_arc)
  NSURLCache* sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
#else
  NSURLCache* sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"] autorelease];
#endif
  [NSURLCache setSharedURLCache:sharedCache];
  
  self = [super init];
  return self;
}

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    //self.viewController = [[MainViewController alloc] init];
    //return [super application:application didFinishLaunchingWithOptions:launchOptions];
  //
  if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"clientVersion"] isEqualToString:CLIENT_VERSION]) {
    //
  }
  else{
    //
    [[NSUserDefaults standardUserDefaults] setObject:CLIENT_VERSION forKey:@"clientVersion"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* libPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0];
    NSString* libPathNoSync = [libPath stringByAppendingPathComponent:@"NoCloud"];
    NSString* localURI = [libPathNoSync stringByAppendingPathComponent:@"www"];
    NSString *bakURI = [libPathNoSync stringByAppendingPathComponent:@"www_bak"];
    if([fileManager fileExistsAtPath:localURI]){
      if ([fileManager fileExistsAtPath:bakURI]) {
        [fileManager removeItemAtPath:bakURI error:nil];
      }
      [fileManager moveItemAtPath:localURI toPath:bakURI error:nil];
      NSLog(@"%@%@",localURI,bakURI);
    }
  }
  CGRect screenBounds = [[UIScreen mainScreen] bounds];
    //注册高德地图服务
    [self registerAMapAPIKey];
    //注册百度地图服务
    [self registerBaiduMapApi];
#if __has_feature(objc_arc)
  self.window = [[UIWindow alloc] initWithFrame:screenBounds];
#else
  self.window = [[[UIWindow alloc] initWithFrame:screenBounds] autorelease];
#endif
  self.window.autoresizesSubviews = YES;
  
//#if __has_feature(objc_arc)
//  self.viewController = [[MainViewController alloc] init];
//#else
//  self.viewController = [[[MainViewController alloc] init] autorelease];
//#endif
  
  // Set your app's start page by setting the <content src='foo.html' /> tag in config.xml.
  // If necessary, uncomment the line below to override it.
  // self.viewController.startPage = @"index.html";
  
  // NOTE: To customize the view's frame size (which defaults to full screen), override
  // [self.viewController viewWillAppear:] in your view controller.
//  [NSURLProtocol registerClass:[JustepURLProtocol class]];
    
//  self.window.rootViewController = self.viewController;
   BGLoginViewController *loginVC = [[BGLoginViewController alloc] initWithNibName:@"BGLoginViewController" bundle:nil];
   UINavigationController *naVC = [[CustomNavigationController alloc] initWithRootViewController:loginVC];
   self.window.rootViewController = naVC;
    
  [self.window makeKeyAndVisible];
  
  return YES;
}

#pragma mark - AMapAPI
- (void)registerAMapAPIKey{
    NSString *APIKey = @"2344c9b2271cd4a26f1b86cb4deee2cb";
    if ([APIKey length] == 0){
        NSString *reason = [NSString stringWithFormat:@"apiKey为空，请检查key是否正确设置。"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:reason delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    [AMapServices sharedServices].apiKey = (NSString *)APIKey;
}

#pragma mark - BaiduMapAPI
- (void)registerBaiduMapApi{
    // 要使用百度地图，请先启动BaiduMapManager
//    _mapManager = [[BMKMapManager alloc]init];
//    [[BMKLocationAuth sharedInstance] checkPermisionWithKey:@"h9c5UNS4vqDm1c6E8aS6jtjbN6SQxPjS" authDelegate:self];
//    BOOL ret = [_mapManager start:@"h9c5UNS4vqDm1c6E8aS6jtjbN6SQxPjS" generalDelegate:self];
//    if (!ret) {
//        NSLog(@"manager start failed!");
//    }
}

@end
