//
//  BGLoginViewController.m
//  变电所运维
//
//  Created by Acrel on 2019/5/16.
//

#import "BGLoginViewController.h"
#import "UIColor+BGExtension.h"
#import "CustomMainTBViewController.h"

@interface BGLoginViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *hbgView;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *usenameTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;

@end

@implementation BGLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //初始化页面
    [self creatView];
    // Do any additional setup after loading the view from its nib.
}

- (void)creatView{
    self.navigationController.navigationBar.hidden = YES;
    // - 初始化
    //渐变色：87 178 247   57 124 207
    [self.hbgView.layer addSublayer:[UIColor setGradualChangingColor:self.hbgView fromColor:@"#57B2F7" toColor:@"#397CCF"]];
    UIImageView *imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"login-p"]];
    imageV.frame = CGRectMake((SCREEN_WIDTH-imageV.frame.size.width)/2, 40, imageV.frame.size.width, imageV.frame.size.height);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, imageV.frame.size.height+45, SCREEN_WIDTH, 50)];
    label.text = NSLocalizedString(@"LoginText",nil);
    label.textAlignment = NSTextAlignmentCenter;
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:23]];
    label.textColor = [UIColor whiteColor];
    [self.hbgView addSubview:imageV];
    [self.hbgView addSubview:label];
    [self.signInBtn.layer addSublayer:[UIColor setGradualChangingColor:self.signInBtn fromColor:@"#57B2F7" toColor:@"#397CCF"]];
    [self.signInBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19.f]];
    self.signInBtn.layer.cornerRadius = self.signInBtn.frame.size.height/2;
    self.signInBtn.layer.masksToBounds = YES;
//    self.signInBtn.layer.shadowOffset = CGSizeMake(0, 3);
//    self.signInBtn.layer.shadowOpacity = 0.6;
//    self.signInBtn.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.signInBtn.frame].CGPath;
    self.addressTextField.delegate = self;
    self.usenameTextField.delegate = self;
    self.pwdTextField.delegate = self;
    self.addressTextField.placeholder = NSLocalizedString(@"serverAddressText",nil);
    self.usenameTextField.placeholder = NSLocalizedString(@"usernameText",nil);
    self.pwdTextField.placeholder = NSLocalizedString(@"passwordText",nil);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
//    [self.pwdTextField addTarget:self action:@selector(keyboardWillChangeFrame:) forControlEvents:UIControlEventEditingDidBegin];
    //输入结束
    [self.pwdTextField addTarget:self action:@selector(textFieldEditEnd) forControlEvents:UIControlEventEditingDidEnd];
}
    
- (IBAction)signInClickEvent:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetService bg_postWithPath:@"http://192.168.112.212:8080/web_manage/login.do?uname=xuhang&upass=666666" params:nil success:^(id respObjc) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //登录接口
        CustomMainTBViewController *mainTBVC = [[CustomMainTBViewController alloc]init];
        [UIApplication sharedApplication].keyWindow.rootViewController = mainTBVC;
        DefLog(@"%@",respObjc);
    } failure:^(id respObjc, NSString *errorCode, NSString *errorMsg) {
        DefLog(@"%@",errorMsg);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [MBProgressHUD showError:errorMsg toView:self.view withAfterDelay:2.0f];
        CustomMainTBViewController *mainTBVC = [[CustomMainTBViewController alloc]init];
        [UIApplication sharedApplication].keyWindow.rootViewController = mainTBVC;
        DefLog(@"%@",respObjc);
    }];
//    [NetService bg_postWithPath:@"http://192.168.112.210:8080/web_manage/login.do" params:@{@"":@""}
   
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    return [textField resignFirstResponder];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)textFieldEditEnd{
    [self.view endEditing:YES];
}

#pragma mark 键盘处理
- (void)keyboardWillChangeFrame:(NSNotification *)note{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    if ([keywindow performSelector:@selector(firstResponder)] == self.pwdTextField) {
        // 取出键盘最终的frame
        CGRect rect = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        // 取出键盘弹出需要花费的时间
        double duration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        // 修改transform
        [UIView animateWithDuration:duration animations:^{
            CGFloat ty = [UIScreen mainScreen].bounds.size.height - rect.origin.y;
            self.view.transform = CGAffineTransformMakeTranslation(0, - ty);
        }];
    }
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.view.frame =CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
