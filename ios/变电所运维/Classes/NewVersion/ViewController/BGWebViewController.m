//
//  BGWebViewController.m
//  变电所运维
//
//  Created by Acrel on 2019/5/16.
//

#import "BGWebViewController.h"

@interface BGWebViewController ()<WKUIDelegate>
@property(nonatomic, strong) WKWebView *wkwebview;
@end

@implementation BGWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.homeButton.hidden = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    //http://192.168.112.210:8080/web_manage/login.jsp
    self.wkwebview = [[WKWebView alloc] initWithFrame:self.view.bounds];
    [self.wkwebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://192.168.112.212:8080/web_manage/login.jsp"]]];
    self.wkwebview.UIDelegate = self;
    [self.wkwebview addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
    [self.view addSubview:self.wkwebview];
    
    // Do any additional setup after loading the view from its nib.
}

//WkWebView的 回调
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"title"]) {
        if (object == self.wkwebview) {
            self.title = self.wkwebview.title;
            if (!self.title.length) {
                self.title = @"Web页面";
            }
        } else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
    
}

- (void)moreButtonAction:(UIButton *)moreBtn{
    
}

- (void)dealloc{
    [_wkwebview removeObserver:self forKeyPath:@"title"];
}

@end
