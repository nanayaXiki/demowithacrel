//
//  ProjectDefine.h
//  变电所运维
//
//  Created by Acrel on 2019/5/16.
//

#ifndef ProjectDefine_h
#define ProjectDefine_h

//打印log
#if 1
#define DefLog(format,...) NSLog(format,##__VA_ARGS__)
#else
#define DefLog(format,...)
#endif

#define COLOR_BACKGROUND DefColorFromRGB(241, 241, 241, 1)
#define DefColorFromRGB(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]
//系统版本判断
#define UIDEVICE_SYSTEMVERSION  [[UIDevice currentDevice].systemVersion floatValue]
#define iOS11 [[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0
#define iOS9 [[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0
#define iOS8 [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0
#define iOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
//屏幕宽高
#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

//判断机型
#define IS_IPHONE_X (SCREEN_HEIGHT == 812.0f) ? YES : NO

#define BGAdjustsScrollViewInsetNever(controller,view) if(@available(iOS 11.0, *)) {view.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;} else if([controller isKindOfClass:[UIViewController class]]) {controller.automaticallyAdjustsScrollViewInsets = false;}

#define BGHeightCoefficient (SCREEN_HEIGHT == 812.0 ? 667.0/667.0 : kWJScreenHeight/667.0)

#define BGSafeAreaTopHeight (SCREEN_HEIGHT == 812.0 ? 88 : 64)

#define BGSafeAreaBottomHeight (SCREEN_HEIGHT == 812.0 ? 20 : 0)

//bg弱引用
#define BGWeakSelf __weak typeof(self) weakSelf = self

//本地化语言
#define DefLocalizedString(__str) NSLocalizedString(__str, nil)

//快速弹出alertView
#define DefQuickAlert(__str,__tar) {UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:__str message:nil delegate:__tar cancelButtonTitle:DefLocalizedString(@"确定") otherButtonTitles:nil, nil];[alertView show];}

//底部菜单栏
#define BottomTabBarHeight (49)
#define DefCellHeight (SCREEN_HEIGHT == 480)?44:((SCREEN_HEIGHT == 568)?50:((SCREEN_HEIGHT == 667)?52:((SCREEN_HEIGHT == 736)?60:((SCREEN_HEIGHT == 736)?60:68))))

//基础url
#define BASE_URL @"http://192.168.112.212:8080/web_manage"

#endif /* ProjectDefine_h */
